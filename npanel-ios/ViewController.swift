import UIKit
import WebKit;

class ViewController: UIViewController, WKNavigationDelegate,WKUIDelegate {

  var webView: WKWebView!

  override func loadView() {
//    view.addSubview(webView)

    let webConfiguration = WKWebViewConfiguration()
    webView = WKWebView(frame: .zero, configuration: webConfiguration)
    webView.uiDelegate = self
    webView.navigationDelegate = self

    let url = URL(string: "http://schpa.local:3449/cards.html")!
    let myRequest = URLRequest(url: url)
    webView.load(myRequest)
    view = webView
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .red;
    print("viewDidLoad")
    print(view)
    print("loadView")
  }

  func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    print(error.localizedDescription)
  }

  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    print("start to load")
  }

  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    print("finish to load")
  }

}

